const mongoose = require('mongoose')
mongoose.connect('mongodb://admin:password@localhost/mydb', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connect')
})

const Schema = mongoose.Schema
const userSchema = new Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    unique: true
  },
  gender: {
    type: String,
    enum: ['M', 'F']
  }
})

module.exports = mongoose.model('User', userSchema)
